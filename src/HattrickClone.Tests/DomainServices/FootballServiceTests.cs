using HattrickClone.Domain;
using HattrickClone.DomainServices.Services;
using HattrickClone.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HattrickClone.Tests
{
    [TestClass]
    public class FootballServiceTests
    {
        #region GetFootballers

        [TestMethod]
        public async Task GetFootballers_When_FootballersExist_Then_ItReturnsAllFootballers()
        {
            // Arrange
            var autoMocker = new AutoMocker();

            var expectedResult = new List<Footballer> { new Footballer(), new Footballer() };

            var persistenceService = autoMocker.GetMock<IPersistenceService>();
            persistenceService.Setup(x => x.GetFootballers()).Returns(Task.FromResult<IEnumerable<Footballer>>(expectedResult));

            // Act
            var target = autoMocker.CreateInstance<FootballerService>();
            var result = await target.GetFootballers();

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        #endregion

        #region MyRegion

        [TestMethod]
        public async Task GetFootballer_When_FootballerExist_Then_ItReturnsFootballer()
        {
            // Arrange
            var autoMocker = new AutoMocker();

            var id = Guid.NewGuid();
            var expectedResult = new Footballer();

            var persistenceService = autoMocker.GetMock<IPersistenceService>();
            persistenceService.Setup(x => x.GetFootballerById(id)).Returns(Task.FromResult(expectedResult));

            // Act
            var target = autoMocker.CreateInstance<FootballerService>();
            var result = await target.GetFootballer(id);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public async Task GetFootballer_When_FootballerDoesNotExist_Then_ItReturnsNull()
        {
            // Arrange
            var autoMocker = new AutoMocker();

            var id = Guid.NewGuid();
            var expectedResult = new Footballer();

            var persistenceService = autoMocker.GetMock<IPersistenceService>();

            // Act
            var target = autoMocker.CreateInstance<FootballerService>();
            var result = await target.GetFootballer(id);

            // Assert
            persistenceService.Verify(x => x.GetFootballerById(id), Times.Once);
            Assert.IsNull(result);
        }

        #endregion
    }
}
