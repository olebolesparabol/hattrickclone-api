﻿using HattrickClone.Domain;
using HattrickClone.DomainServices.Generators;
using HattrickClone.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HattrickClone.DomainServices.Services
{
    public class FootballerService : IFootballerService
    {
        private readonly IPersistenceService _persistenceService;
        private readonly IFootballerGenerator _footballGenerator;

        public FootballerService(IPersistenceService persistenceService, IFootballerGenerator footballGenerator)
        {
            _persistenceService = persistenceService;
            _footballGenerator = footballGenerator;
        }

        public async Task GenerateFootballers(int amount)
        {
            var newFootballers = new List<Footballer>();

            for (int i = 0; i < amount; i++)
            {
                newFootballers.Add(_footballGenerator.Generate());
            }

            await _persistenceService.SaveFootballers(newFootballers);
        }

        public async Task<Footballer> GetFootballer(Guid id)
        {
            return await _persistenceService.GetFootballerById(id);
        }

        public async Task<IEnumerable<Footballer>> GetFootballers()
        {
            return await _persistenceService.GetFootballers();
        }
    }
}
