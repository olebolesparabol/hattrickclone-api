﻿using HattrickClone.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HattrickClone.DomainServices.Services
{
    public interface IFootballerService
    {
        Task<Footballer> GetFootballer(Guid id);
        Task<IEnumerable<Footballer>> GetFootballers();
        Task GenerateFootballers(int amount);
    }
}