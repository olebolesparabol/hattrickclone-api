﻿using HattrickClone.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace HattrickClone.DomainServices.Generators
{
    public interface IFootballerGenerator
    {
        Footballer Generate();
    }
}
