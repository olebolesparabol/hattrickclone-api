﻿using HattrickClone.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace HattrickClone.DomainServices.Generators
{
    public class FootballerGenerator : IFootballerGenerator
    {
        private readonly IList<string> _firstNames;
        private readonly IList<string> _lastNames;
        private readonly IList<string> _nationalities;

        public FootballerGenerator()
        {
            _firstNames = new List<string> { "Anders", "Benjamin", "Kasper", "Morten", "Thomas" };
            _lastNames = new List<string> { "Jensen", "Hansen", "Olsen", "Bundgaard", "Skov" };
            _nationalities = new List<string> { "Danish", "Swedish", "Norwegian" };
        }

        public Footballer Generate()
        {
            var random = new Random();

            return new Footballer
            {
                Id = Guid.NewGuid(),
                Name = GenerateName(),
                Nationality = _nationalities[random.Next(0, _nationalities.Count)],
                Defence = random.Next(0, 5),
                Passing = random.Next(0, 5),
                Playmaking = random.Next(0, 5),
                Scoring = random.Next(0, 5)
            };
        }

        private string GenerateName()
        {
            var random = new Random();

            var firstName = _firstNames[random.Next(0, _firstNames.Count)];
            var lastName = _lastNames[random.Next(0, _lastNames.Count)];

            return $"{firstName} {lastName}";
        }
    }
}
