﻿using System;

namespace HattrickClone.Domain
{
    public class Footballer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public int Defence { get; set; }
        public int Playmaking { get; set; }
        public int Passing { get; set; }
        public int Scoring { get; set; }
    }
}
