﻿using HattrickClone.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HattrickClone.Persistence
{
    public interface IPersistenceService
    {
        Task<IEnumerable<Footballer>> GetFootballers();
        Task<Footballer> GetFootballerById(Guid id);
        Task SaveFootballers(IEnumerable<Footballer> footballers);
    }
}