﻿using HattrickClone.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HattrickClone.Persistence
{
    public class PersistenceService : IPersistenceService
    {
        ICollection<Footballer> _footballerCache;

        public PersistenceService()
        {
            _footballerCache = new List<Footballer>();
        }

        public Task<IEnumerable<Footballer>> GetFootballers()
        {
            // Faking async/await here
            return Task.FromResult<IEnumerable<Footballer>>(_footballerCache);
        }

        public Task<Footballer> GetFootballerById(Guid id)
        {
            // Faking async/await here
            return Task.FromResult(_footballerCache.SingleOrDefault(x => x.Id == id)); // Ensure uniqueness of id in database
        }

        public Task SaveFootballers(IEnumerable<Footballer> footballers)
        {
            foreach (var footballer in footballers)
            {
                _footballerCache.Add(footballer); // If its a List<T>, use AddRange
            }

            // Faking async/await here
            return Task.CompletedTask;
        }
    }
}
