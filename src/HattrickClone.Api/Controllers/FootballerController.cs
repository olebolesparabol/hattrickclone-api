﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HattrickClone.Domain;
using HattrickClone.DomainServices.Services;

namespace HattrickClone.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FootballerController : ControllerBase
    {
        private readonly IFootballerService _footballerService;
        private readonly ILogger<FootballerController> _logger;

        public FootballerController(IFootballerService footballerService, ILogger<FootballerController> logger)
        {
            _footballerService = footballerService;
            _logger = logger;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<Footballer>> GetAllFootballers()
        {
            return await _footballerService.GetFootballers();
        }

        [HttpGet]
        [Route("[action]/{id}")]
        public async Task<Footballer> GetFootballer(Guid id)
        {
            return await _footballerService.GetFootballer(id);
        }

        [HttpPost]
        [Route("[action]/{amount}")]
        public async Task GenerateFootballers(int amount)
        {
            await _footballerService.GenerateFootballers(amount);
        }
    }
}
